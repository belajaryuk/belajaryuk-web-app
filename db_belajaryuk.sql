-- phpMyAdmin SQL Dump
-- version 4.9.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Nov 26, 2019 at 03:06 AM
-- Server version: 10.4.6-MariaDB
-- PHP Version: 7.3.8

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_belajaryuk`
--

-- --------------------------------------------------------

--
-- Table structure for table `admin`
--

CREATE TABLE `admin` (
  `username` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `admin`
--

INSERT INTO `admin` (`username`, `password`) VALUES
('admin', 'admin');

-- --------------------------------------------------------

--
-- Table structure for table `kursus`
--

CREATE TABLE `kursus` (
  `id` int(50) NOT NULL,
  `nama_kursus` varchar(100) NOT NULL,
  `id_mentor` int(50) NOT NULL,
  `harga` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `kursus`
--

INSERT INTO `kursus` (`id`, `nama_kursus`, `id_mentor`, `harga`) VALUES
(1, 'Belajar Bahasa Indonesia', 1, 550),
(2, 'Matematika', 2, 500);

-- --------------------------------------------------------

--
-- Table structure for table `materi`
--

CREATE TABLE `materi` (
  `id` int(20) NOT NULL,
  `judul_materi` varchar(100) NOT NULL,
  `isi_materi` text NOT NULL,
  `id_kursus` int(20) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `materi`
--

INSERT INTO `materi` (`id`, `judul_materi`, `isi_materi`, `id_kursus`) VALUES
(1, 'Apa itu Bahasa Indonesia?', '<p><b>Bahasa Indonesia</b> adalah <a href=\"/wiki/Bahasa_Melayu\" title=\"Bahasa Melayu\">bahasa Melayu</a> yang dijadikan sebagai <a href=\"/wiki/Bahasa\" title=\"Bahasa\">bahasa</a> <a href=\"/wiki/Bahasa_resmi\" title=\"Bahasa resmi\">resmi</a> <a href=\"/wiki/Republik_Indonesia\" class=\"mw-redirect\" title=\"Republik Indonesia\">Republik Indonesia</a><sup id=\"cite_ref-1\" class=\"reference\"><a href=\"#cite_note-1\">[1]</a></sup> dan <a href=\"/wiki/Daftar_bahasa_di_Indonesia\" title=\"Daftar bahasa di Indonesia\">bahasa persatuan</a> <a href=\"/wiki/Orang_Indonesia\" title=\"Orang Indonesia\">bangsa Indonesia</a>.<sup id=\"cite_ref-2\" class=\"reference\"><a href=\"#cite_note-2\">[2]</a></sup> Bahasa Indonesia diresmikan penggunaannya setelah <a href=\"/wiki/Proklamasi_Kemerdekaan_Indonesia\" title=\"Proklamasi Kemerdekaan Indonesia\">Proklamasi Kemerdekaan Indonesia</a>, tepatnya sehari sesudahnya, bersamaan dengan mulai berlakunya <a href=\"/wiki/UUD_1945\" class=\"mw-redirect\" title=\"UUD 1945\">konstitusi</a>. Di <a href=\"/wiki/Timor_Leste\" title=\"Timor Leste\">Timor Leste</a>, bahasa Indonesia berstatus sebagai <a href=\"/wiki/Bahasa_kerja\" title=\"Bahasa kerja\">bahasa kerja</a>.\r\n</p>\r\n<br>\r\n<p>Dari sudut pandang <a href=\"/wiki/Linguistik\" title=\"Linguistik\">linguistik</a>, bahasa Indonesia adalah salah satu dari banyak varietas <a href=\"/wiki/Bahasa_Melayu\" title=\"Bahasa Melayu\">bahasa Melayu</a>.<sup id=\"cite_ref-3\" class=\"reference\"><a href=\"#cite_note-3\">[3]</a></sup> Dasar yang dipakai sebagai fondasi bahasa Indonesia baku adalah bahasa Melayu Tinggi (\"Riau\").<sup id=\"cite_ref-:0_4-0\" class=\"reference\"><a href=\"#cite_note-:0-4\">[4]</a></sup><sup id=\"cite_ref-Sneddon_5-0\" class=\"reference\"><a href=\"#cite_note-Sneddon-5\">[5]</a></sup> Dalam perkembangannya ia mengalami perubahan akibat penggunaannya sebagai bahasa kerja di lingkungan administrasi kolonial dan berbagai proses pembakuan sejak awal abad ke-20. Penamaan \"bahasa Indonesia\" diawali sejak dicanangkannya <a href=\"/wiki/Sumpah_Pemuda\" title=\"Sumpah Pemuda\">Sumpah Pemuda</a>, 28 Oktober 1928, untuk menghindari kesan \"imperialisme bahasa\" apabila nama bahasa Melayu tetap digunakan.<sup id=\"cite_ref-6\" class=\"reference\"><a href=\"#cite_note-6\">[6]</a></sup> Proses ini menyebabkan berbedanya bahasa Indonesia saat ini dari varian bahasa Melayu yang digunakan di Riau maupun <a href=\"/wiki/Semenanjung_Malaya\" title=\"Semenanjung Malaya\">Semenanjung Malaya</a>. Hingga saat ini, bahasa Indonesia merupakan bahasa yang hidup, yang terus menghasilkan kata-kata baru, baik melalui penciptaan maupun penyerapan dari <a href=\"/wiki/Bahasa_daerah\" title=\"Bahasa daerah\">bahasa daerah</a> dan <a href=\"/wiki/Bahasa_asing\" title=\"Bahasa asing\">bahasa asing</a>.\r\n</p>\r\n<br>\r\n<p>Meskipun dipahami dan dituturkan oleh lebih dari 90% warga Indonesia, bahasa Indonesia bukanlah <a href=\"/wiki/Bahasa_ibu\" title=\"Bahasa ibu\">bahasa ibu</a> bagi kebanyakan penuturnya. Sebagian besar warga Indonesia menggunakan salah satu dari <a href=\"/wiki/Daftar_bahasa_di_Indonesia\" title=\"Daftar bahasa di Indonesia\">748 bahasa yang ada di Indonesia</a> sebagai <a href=\"/wiki/Bahasa_ibu\" title=\"Bahasa ibu\">bahasa ibu</a>.<sup id=\"cite_ref-7\" class=\"reference\"><a href=\"#cite_note-7\">[7]</a></sup> Istilah \"bahasa Indonesia\" paling umum dikaitkan dengan <a href=\"/wiki/Bahasa_baku\" title=\"Bahasa baku\">bahasa baku</a> yang digunakan dalam situasi formal.<sup id=\"cite_ref-:0_4-1\" class=\"reference\"><a href=\"#cite_note-:0-4\">[4]</a></sup> Varietas baku tersebut berhubungan <a href=\"/wiki/Diglosia\" title=\"Diglosia\">diglosik</a> dengan bentuk-bentuk bahasa Melayu vernakular yang digunakan sebagai peranti komunikasi sehari-hari.<sup id=\"cite_ref-:0_4-2\" class=\"reference\"><a href=\"#cite_note-:0-4\">[4]</a></sup> Artinya, penutur bahasa Indonesia kerap kali menggunakan versi sehari-hari (kolokial) dan/atau mencampuradukkan dengan dialek Melayu lainnya atau bahasa ibunya. Meskipun demikian, bahasa Indonesia digunakan sangat luas di perguruan-perguruan, di media massa, sastra, perangkat lunak, surat-menyurat resmi, dan berbagai forum publik lainnya,<sup id=\"cite_ref-8\" class=\"reference\"><a href=\"#cite_note-8\">[8]</a></sup> sehingga dapatlah dikatakan bahwa bahasa Indonesia digunakan oleh semua warga Indonesia.\r\n</p>', 1),
(2, 'What\'s Math?', '<p><b>Mathematics</b> (from <a href=\"/wiki/Ancient_Greek\" title=\"Ancient Greek\">Greek</a> ?????? <i>máth?ma</i>, \"knowledge, study, learning\") includes the study of such topics as <a href=\"/wiki/Quantity\" title=\"Quantity\">quantity</a> (<a href=\"/wiki/Number_theory\" title=\"Number theory\">number theory</a>),<sup id=\"cite_ref-OED_2-0\" class=\"reference\"><a href=\"#cite_note-OED-2\">[1]</a></sup> <a href=\"/wiki/Mathematical_structure\" title=\"Mathematical structure\">structure</a> (<a href=\"/wiki/Algebra\" title=\"Algebra\">algebra</a>),<sup id=\"cite_ref-Kneebone_3-0\" class=\"reference\"><a href=\"#cite_note-Kneebone-3\">[2]</a></sup> <a href=\"/wiki/Space\" title=\"Space\">space</a> (<a href=\"/wiki/Geometry\" title=\"Geometry\">geometry</a>),<sup id=\"cite_ref-OED_2-1\" class=\"reference\"><a href=\"#cite_note-OED-2\">[1]</a></sup> and <a href=\"/wiki/Calculus\" title=\"Calculus\">change</a> (<a href=\"/wiki/Mathematical_analysis\" title=\"Mathematical analysis\">mathematical analysis</a>).<sup id=\"cite_ref-LaTorre_4-0\" class=\"reference\"><a href=\"#cite_note-LaTorre-4\">[3]</a></sup><sup id=\"cite_ref-Ramana_5-0\" class=\"reference\"><a href=\"#cite_note-Ramana-5\">[4]</a></sup><sup id=\"cite_ref-Ziegler_6-0\" class=\"reference\"><a href=\"#cite_note-Ziegler-6\">[5]</a></sup> It has no generally accepted <a href=\"/wiki/Definition\" title=\"Definition\">definition</a>.<sup id=\"cite_ref-Mura_7-0\" class=\"reference\"><a href=\"#cite_note-Mura-7\">[6]</a></sup><sup id=\"cite_ref-Runge_8-0\" class=\"reference\"><a href=\"#cite_note-Runge-8\">[7]</a></sup>\r\n</p>\r\n<br>\r\n<p>Mathematicians seek and use <a href=\"/wiki/Patterns\" class=\"mw-redirect\" title=\"Patterns\">patterns</a><sup id=\"cite_ref-future_9-0\" class=\"reference\"><a href=\"#cite_note-future-9\">[8]</a></sup><sup id=\"cite_ref-devlin_10-0\" class=\"reference\"><a href=\"#cite_note-devlin-10\">[9]</a></sup> to formulate new <a href=\"/wiki/Conjecture\" title=\"Conjecture\">conjectures</a>; they resolve the truth or falsity of conjectures by <a href=\"/wiki/Mathematical_proof\" title=\"Mathematical proof\">mathematical proof</a>. When mathematical structures are good models of real phenomena, mathematical reasoning can be used to provide insight or predictions about nature. Through the use of <a href=\"/wiki/Abstraction_(mathematics)\" title=\"Abstraction (mathematics)\">abstraction</a> and <a href=\"/wiki/Logic\" title=\"Logic\">logic</a>, mathematics developed from <a href=\"/wiki/Counting\" title=\"Counting\">counting</a>, <a href=\"/wiki/Calculation\" title=\"Calculation\">calculation</a>, <a href=\"/wiki/Measurement\" title=\"Measurement\">measurement</a>, and the systematic study of the <a href=\"/wiki/Shape\" title=\"Shape\">shapes</a> and <a href=\"/wiki/Motion_(physics)\" class=\"mw-redirect\" title=\"Motion (physics)\">motions</a> of <a href=\"/wiki/Physical_objects\" class=\"mw-redirect\" title=\"Physical objects\">physical objects</a>. Practical mathematics has been a human activity from as far back as <a href=\"/wiki/History_of_Mathematics\" class=\"mw-redirect\" title=\"History of Mathematics\">written records</a> exist. The <a href=\"/wiki/Research\" title=\"Research\">research</a> required to solve mathematical problems can take years or even centuries of sustained inquiry.\r\n</p>\r\n<br>\r\n<p><a href=\"/wiki/Logic\" title=\"Logic\">Rigorous arguments</a> first appeared in <a href=\"/wiki/Greek_mathematics\" title=\"Greek mathematics\">Greek mathematics</a>, most notably in <a href=\"/wiki/Euclid\" title=\"Euclid\">Euclid</a>\'s <i><a href=\"/wiki/Euclid%27s_Elements\" title=\"Euclid\'s Elements\">Elements</a></i>.<sup id=\"cite_ref-11\" class=\"reference\"><a href=\"#cite_note-11\">[10]</a></sup> Since the pioneering work of <a href=\"/wiki/Giuseppe_Peano\" title=\"Giuseppe Peano\">Giuseppe Peano</a> (1858–1932), <a href=\"/wiki/David_Hilbert\" title=\"David Hilbert\">David Hilbert</a> (1862–1943), and others <a href=\"/wiki/Foundations_of_mathematics\" title=\"Foundations of mathematics\">on axiomatic systems in the late 19th&nbsp;century</a>, it has become customary to view mathematical research as establishing <a href=\"/wiki/Truth\" title=\"Truth\">truth</a> by <a href=\"/wiki/Mathematical_rigor\" class=\"mw-redirect\" title=\"Mathematical rigor\">rigorous</a> <a href=\"/wiki/Deductive_reasoning\" title=\"Deductive reasoning\">deduction</a> from appropriately chosen <a href=\"/wiki/Axiom\" title=\"Axiom\">axioms</a> and <a href=\"/wiki/Definition\" title=\"Definition\">definitions</a>. Mathematics developed at a relatively slow pace until the <a href=\"/wiki/Renaissance\" title=\"Renaissance\">Renaissance</a>, when mathematical innovations interacting with new <a href=\"/wiki/Timeline_of_scientific_discoveries\" title=\"Timeline of scientific discoveries\">scientific discoveries</a> led to a rapid increase in the rate of mathematical discovery that has continued to the present day.<sup id=\"cite_ref-12\" class=\"reference\"><a href=\"#cite_note-12\">[11]</a></sup>\r\n</p>\r\n<br>\r\n<p>Mathematics is essential in many fields, including <a href=\"/wiki/Natural_science\" title=\"Natural science\">natural science</a>, <a href=\"/wiki/Engineering\" title=\"Engineering\">engineering,</a> <a href=\"/wiki/Medicine\" title=\"Medicine\">medicine,</a> <a href=\"/wiki/Finance\" title=\"Finance\">finance,</a> and the <a href=\"/wiki/Social_sciences\" class=\"mw-redirect\" title=\"Social sciences\">social sciences</a>. <a href=\"/wiki/Applied_mathematics\" title=\"Applied mathematics\">Applied mathematics</a> has led to entirely new mathematical disciplines, such as <a href=\"/wiki/Statistics\" title=\"Statistics\">statistics</a> and <a href=\"/wiki/Game_theory\" title=\"Game theory\">game theory</a>. Mathematicians engage in <a href=\"/wiki/Pure_mathematics\" title=\"Pure mathematics\">pure mathematics</a> (mathematics for its own sake) without having any application in mind, but practical applications for what began as pure mathematics are often discovered later.<sup id=\"cite_ref-13\" class=\"reference\"><a href=\"#cite_note-13\">[12]</a></sup><sup id=\"cite_ref-wigner1960_14-0\" class=\"reference\"><a href=\"#cite_note-wigner1960-14\">[13]</a></sup>\r\n</p>', 2),
(3, 'Sejarah Bahasa Indonesia', '<p><strong>Sejarah Bahasa Indonesia</strong> – Bahasa adalah identitas suatu bangsa yang digunakan untuk berkomunikasi dnegan bangsa lain. Dimana setiap bangsa memiliki bahasa yang berbeda-beda dengan ciri khas dan asal-usul masing-masing. Begitu juga dengan bahasa Indonesia. Sejarah bahasa Indonesia sendiri tidak lepas dari bahasa Melayu.</p>\r\n<br>\r\n<p>Bahasa Indonesia lahir pada tanggal 28 Oktober 1928. Dimana pada tanggal tersebut, para pemuda dari seluruh pelosok Nusantara berkumpul dan berikrar Sumpah Pemuda dengan isi :</p>\r\n<br>\r\n<ol>\r\n<li>Bertumpah darah yang satu, tanah Indonesia</li>\r\n<li>Berbangsa yang satu, bangsa Indonesia, dan</li>\r\n<li>Menjunjung bahasa persatuan, bahasa Indonesia</li>\r\n</ol>\r\n<br>\r\n<p>Dengan Sumpah Pemuda itulah, bahasa Indonesia kemudian dikukuhkan menjadi bahasa nasional. Kemudian pada tanggal 18 Agustus 1945, bahasa Indonesia menjadi bahasa negara dan terkandung dalam UUD 1945 Bab XV, Pasal 36.</p>', 1);

-- --------------------------------------------------------

--
-- Table structure for table `mentor`
--

CREATE TABLE `mentor` (
  `id` int(50) NOT NULL,
  `email_mentor` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `nama_lengkap_mentor` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `mentor`
--

INSERT INTO `mentor` (`id`, `email_mentor`, `password`, `nama_lengkap_mentor`) VALUES
(1, 'anyageraldine@gmail.com', 'anya', 'Anya Geraldine'),
(2, 'adhistyzara@gmail.com', 'zara', 'Adhisty Zara');

-- --------------------------------------------------------

--
-- Table structure for table `quiz`
--

CREATE TABLE `quiz` (
  `id` int(50) NOT NULL,
  `nama_quiz` varchar(100) NOT NULL,
  `id_kursus` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `quiz_nilai`
--

CREATE TABLE `quiz_nilai` (
  `id` int(50) NOT NULL,
  `id_quiz` int(50) NOT NULL,
  `id_user` int(50) NOT NULL,
  `id_mentor` int(50) NOT NULL,
  `nilai` int(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(200) NOT NULL,
  `email` varchar(200) NOT NULL,
  `password` varchar(200) NOT NULL,
  `nama_lengkap` varchar(200) DEFAULT NULL,
  `nama_panggilan` varchar(100) DEFAULT NULL,
  `tgl_lahir` varchar(191) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `nama_lengkap`, `nama_panggilan`, `tgl_lahir`) VALUES
(1, 'andhikurniawan@gmail.com', '123', 'Andhi Kurniawan', 'Andhi', '0000-00-00'),
(5, '1@1.c', '1', 'Test User', 'Pengguna', '02/14/2000'),
(6, 'adhit@a.co', 'a', 'Adhitya K', 'Adhit', '11/20/2019');

-- --------------------------------------------------------

--
-- Table structure for table `users_kursus`
--

CREATE TABLE `users_kursus` (
  `id` int(50) NOT NULL,
  `id_user` int(50) NOT NULL,
  `id_kursus` int(50) NOT NULL,
  `id_mentor` int(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_kursus`
--

INSERT INTO `users_kursus` (`id`, `id_user`, `id_kursus`, `id_mentor`) VALUES
(1, 5, 1, 1),
(5, 1, 1, 2),
(6, 1, 2, 2),
(7, 6, 1, 1),
(8, 6, 2, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admin`
--
ALTER TABLE `admin`
  ADD PRIMARY KEY (`username`);

--
-- Indexes for table `kursus`
--
ALTER TABLE `kursus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `materi`
--
ALTER TABLE `materi`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `mentor`
--
ALTER TABLE `mentor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz`
--
ALTER TABLE `quiz`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `quiz_nilai`
--
ALTER TABLE `quiz_nilai`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_kursus`
--
ALTER TABLE `users_kursus`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `kursus`
--
ALTER TABLE `kursus`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `materi`
--
ALTER TABLE `materi`
  MODIFY `id` int(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `mentor`
--
ALTER TABLE `mentor`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `quiz_nilai`
--
ALTER TABLE `quiz_nilai`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(200) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `users_kursus`
--
ALTER TABLE `users_kursus`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
