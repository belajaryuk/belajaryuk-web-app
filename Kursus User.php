<!DOCTYPE html>
<html lang="en">
<head>
  <title>Kursus Saya</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
<style>
  body {font-family: "Lato", sans-serif;}

.sidebar {
  height: 100%;
  width: 200px;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color:#24d4d4;
  overflow-x: hidden;
  padding-top: 16px;
  margin-top: 4%;
  

}

.sidebar a {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 20px;
  color:floralwhite;
  display: block;
}

.sidebar a:hover {
  color: cadetblue;
}

.main {
  margin-left: 160px; /* Same as the width of the sidenav */
  padding: 0px 10px;
  margin-top: 4%;
}

@media screen and (max-height: 450px) {
  .sidebar {padding-top: 15px;}
  .sidebar a {font-size: 18px;}
}
.nav-link :hover{
  color: white;
}
</style>
</head>
<body>


<nav class="navbar fixed-top navbar-light" style="background-color: #00D8D6;">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample01" aria-controls="navbarsExample01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a href="#" style="color: white; font-size: 200%; float: left;">BelajarYuk!</a>
  
  <div class="collapse navbar-collapse" id="navbarsExample01">
    <ul class="navbar-nav" id="navbarsExample01">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Menu Kursus.html">Kursus</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Menu Programs.html">Program</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Pengajar.html">Pengajar</a>
      </li>
    </ul>
  </div>

    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white;">
      Account
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
      <a class="dropdown-item" href="#">Profil</a>
      <a class="dropdown-item" href="#">Daftar</a>
      <a class="dropdown-item" href="#">Keluar</a>
    </div>
  
  
</nav>

<br>
<div class="sidebar">
  
 <a href="#home"><i class="fa fa-fw fa-home"></i> Home</a> 
  <a href="#services"><i class="fa fa-fw fa-wrench"></i>Kursus Saya</a>


</div>

    <!-- <ul>
        <li><a href="">Beranda</a></li>
        <li> <a href="">Kursus Saya</a></li>
    </ul> -->
</div>
<div class="main">
<h1 style="text-align: center; font-size:50px;"><i>Kursus yang diikuti :</i></h1>

</div>
</body>
</html>


