<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Quiz</title>
    <script src="jquery-3.4.1.min.js"></script>
    <script type="text/javascript">


        function validasi() {
            var nama = $("input[name='inputName']").val();
            var nim = $("input[name='inputNIM']").val();
            if (nama == "" && nim == "") {   
                alert("Lengkapi nama dan nim anda!");       
            } else {
                
            }
        }

        $(document).ready(function () {
            $("button[type='submit']").click(function () {
                var ques1 = $("input[name='Answer1']:checked").val();
                var ques2 = $("input[name='Answer2']:checked").val();
                var ques3 = $("input[name='Answer3']:checked").val();
                var ques4 = $("input[name='Answer4']:checked").val();
                var ques5 = $("input[name='Answer5']:checked").val();
                var score = 0;
                var tot = 0;
                if (ques1 == "a") {
                    score++;
                }
                if (ques2 == "c") {
                    score++;

                }
                if (ques3 == "c") {
                    score++;

                }
                if (ques4 == "a") {
                    score++;

                }
                if (ques5 == "a") {
                    score++;

                }
                var nama = $("input[name='inputName']").val();
                var nim = $("input[name='inputNIM']").val();
                if (nama =="" && nim =="") {

                } else {
                    tot = score * 20;
                    alert("Nama : " + nama + "\nNim : " + nim + "\nYour score : " + tot);
                }

                window.location.href='daftar_kursus_user.php';
            });

        });

    </script>
    <style>
    body{
    background-color: lightgray;
}
.container{
    margin: auto;
    margin-top: -15px;
    background-color: cornsilk;
    height: 1000px;
    width: 70%;
}
.img{
    width: 28%;
    margin-left: 30px;
    float: left;
}
.form, table {
    float: right;
    margin-right: 18px;
    margin-top: 11px;
}
h1{
    text-align: center;
    padding: 30px;
} 
hr{
    margin-top: 120px;
    width: 92%;
}
.form2{
    float: left;
    margin-left: 30px;
    margin-top: 20px;
}
button[type="submit"]{
    width: 200px;
    margin-left: 90%;
    margin-top: 20%;
    padding: 10px;
    border-radius: 20px;
    background-color: green;
    color: aliceblue;
}
    </style>

</head>

<body>
    <div class="container">
        <h1>QUIZ BAHASA INDONESIA</h1>
        <img class="img" src="EAD.png" alt="logo">
        <form class="form">
            <table>
                <tr>
                    <td><label for="inputName">Nama</label></td>
                    <td>:</td>
                    <td><input type="text" name="inputName"></td>
                </tr>
                <tr>
                    <td><label for="inputNIM">NIM</label></td>
                    <td>:</td>
                    <td><input type="number" name="inputNIM"></td>
                </tr>

            </table>



        </form>
        <hr>
        <form class="form2">


            <label for="Answer1">1. Apakah Kepanjangan HTML ?</label> <br><br>

            <input type="radio" name="Answer1" value="a">A. Hypertext Markup Language <br>


            <input type="radio" name="Answer1" value="b">B. Hypertext Mockup Languange <br>

            <input type="radio" name="Answer1" value="c">C. Hypertext Module Lang <br><br>

            <label for="Answer2">2. HTML yang benar untuk menambahkan warna latar belakang ?</label> <br><br>

            <input type="radio" name="Answer2" value="a">A. &lt;body bg="yellow"&gt; <br>

            <input type="radio" name="Answer2" value="b">B. &lt;body background="yellow"&gt;<br>

            <input type="radio" name="Answer2" value="c">C. &lt;body style="background-color:yellow;"&gt; <br><br>

            <label for="Answer3">3. Di dalam elemen HTML manakah kita menempatkan JavaScript?</label> <br><br>

            <input type="radio" name="Answer3" value="a">A. &lt;scripting&gt; <br>

            <input type="radio" name="Answer3" value="b">B. &lt;javascript&gt; <br>

            <input type="radio" name="Answer3" value="c">C. &lt;script&gt;<br><br>

            <label for="Answer4">4. Bagaimana Anda menulis "Hello World" di sebuah alert box?</label> <br><br>

            <input type="radio" name="Answer4" value="a"> A. alert("Hello World"); <br>

            <input type="radio" name="Answer4" value="b">B. alertBox("Hello World"); <br>

            <input type="radio" name="Answer4" value="c">C. msg("Hello World"); <br> <br>

            <label for="Answer5">5. Apakah kepanjangan CSS ?</label> <br><br>

            <input type="radio" name="Answer5" value="a">A. Cascading Style Sheets <br>

            <input type="radio" name="Answer5" value="b">B. Casading Style Sheets <br>

            <input type="radio" name="Answer5" value="c">C. Canvas Style Sheets <br>

            <button type="submit" onclick="validasi()" >Submit</button>
           
        </form>
        <a href="daftar_kursus_user.php"><button type="submit" style="margin-left:-50%; margin-top:80%; background-color:red;">Back</button></a>

    </div>
</body>

</html>