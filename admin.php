<?php
include "config/connection.php";
session_start();

if (!isset($_SESSION['name'])) {
  echo "<script>alert('Please Login First');window.location.href='Halaman-home.php'</script>";
}

$id = $_SESSION['name'];
$data = mysqli_query($conn, "select * from admin where username='$id'");
while ($d = mysqli_fetch_array($data)) {
    
?>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Daftar User</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
<style>
  body {font-family: "Lato", sans-serif;}

.sidebar {
  height: 100%;
  width: 200px;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color:#24d4d4;
  overflow-x: hidden;
  padding-top: 16px;
  margin-top: 4%;
  

}

.sidebar a {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 20px;
  color:floralwhite;
  display: block;
}

.sidebar a:hover {
  color: cadetblue;
}
.sidebar a:active {
  color: cadetblue;
}

.main {
  margin-left: 160px; /* Same as the width of the sidenav */
  padding: 0px 10px;
  margin-top: 4%;
}

@media screen and (max-height: 450px) {
  .sidebar {padding-top: 15px;}
  .sidebar a {font-size: 18px;}
}
.nav-link :hover{
  color: white;
}
</style>
</head>
<body>

<nav class="navbar fixed-top" style="background-color: #00D8D6;">
        <a class="navbar-brand" href="Halaman-home.php"
            style="font-family: 'Rubik', sans-serif; font-size: 30px; font-weight: 900; color: white">BelajarYuk!</a>
        <button class="navbar-toggler" type="button" data-toggler="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle Navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="d-flex flex-row-reverse bd-highlight">

<div class="nav-item dropdown" style="padding: 10px;" style="font-family: 'Rubik', sans-serif; font-size: 22px;">
    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-family: 'Rubik', sans-serif; font-size: 22px; color:white;">
        Hai, <?= $d['username']; ?>
  </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
        <a class="dropdown-item" href="edit_profile_user.php">Edit Profile</a>
        <a class="dropdown-item" href="logout.php">Logout</a>
    </div>

</div>
</div>    

    </nav>

    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white;">
      Account
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
      <a class="dropdown-item" href="#">Profil</a>
      <a class="dropdown-item" href="#">Daftar user</a>
      <a class="dropdown-item" href="#">Keluar</a>
    </div>
  
  
</nav>

<br>
<div class="sidebar" style="margin-top:5%">
  
 <a href="#home"><i class="fa fa-fw fa-home"></i> Home</a> 
  <a href="admin_user.php"><i class="fa fa-fw fa-wrench"></i>Daftar User</a>
  <a href="daftar_pengajar.php"><i class="fa fa-fw fa-wrench"></i>Daftar Pengajar</a>
</div>
</div>
<div class="main">
<h1 style="text-align: center; font-size:50px; margin-top:8%" >Selamat Datang , <?= $d['username']; ?> !</h1><br>
<div style="width:70%;margin:auto;">
<img src="img/undraw_programmer_imem (1).png" alt="" style="width: 600px; margin-left:10%; margin-right: auto;"><
</div>
</div>
</body>
<?php
}
?>
</html>