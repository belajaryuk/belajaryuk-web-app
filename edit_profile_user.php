<?php
session_start();
if (!isset($_SESSION['email'])) {
    echo "<script>alert('Please Login First');window.location.href='Halaman-home.php'</script>";
}

include "config/connection.php";
$id = $_SESSION['id'];
$data = mysqli_query($conn, "select * from users where id='$id'");
while ($d = mysqli_fetch_array($data)) {
    ?>
    <!DOCTYPE html>
    <html lang="en">

    <head>
        <title><?= $d['nama_panggilan']; ?>'s List Kursus</title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="style.css">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
        <style>
            body {
                font-family: "Lato", sans-serif;
            }

            .sidebar {
                height: 100%;
                width: 200px;
                position: fixed;
                z-index: 1;
                top: 0;
                left: 0;
                background-color: #24d4d4;
                overflow-x: hidden;
                padding-top: 16px;
                margin-top: 4%;


            }

            .sidebar a {
                padding: 6px 8px 6px 16px;
                text-decoration: none;
                font-size: 20px;
                color: floralwhite;
                display: block;
            }

            .sidebar a:hover {
                color: cadetblue;
            }

            .sidebar a:active {
                color: cadetblue;
            }

            .main {
                margin-left: 160px;
                /* Same as the width of the sidenav */
                padding: 0px 10px;
                margin-top: 4%;
            }

            @media screen and (max-height: 450px) {
                .sidebar {
                    padding-top: 15px;
                }

                .sidebar a {
                    font-size: 18px;
                }
            }

            .nav-link :hover {
                color: white;
            }
        </style>
    </head>

    <body>

        <nav class="navbar fixed-top" style="background-color: #00D8D6;">
            <a class="navbar-brand" href="Halaman-home.php" style="font-family: 'Rubik', sans-serif; font-size: 30px; font-weight: 900; color: white">BelajarYuk!</a>
            <button class="navbar-toggler" type="button" data-toggler="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle Navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="d-flex flex-row-reverse bd-highlight">

                <div class="nav-item dropdown" style="padding: 10px;" style="font-family: 'Rubik', sans-serif; font-size: 22px;">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="font-family: 'Rubik', sans-serif; font-size: 22px; color:white;">
                        Hai, <?= $d['nama_panggilan'] ?>
                    </a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                        <a class="dropdown-item" href="edit_profile_user.php">Edit Profile</a>
                        <a class="dropdown-item" href="logout.php">Logout</a>
                    </div>

                </div>
            </div>

        </nav>

        <br>
        <div class="sidebar">

            <a href="dashboard-home.php"><i class="fa fa-fw fa-home"></i> <strong>Home</strong></a>
            <a href="daftar_kursus_user.php"><i class="fa fa-fw fa-wrench"></i>Daftar Kursus</a>


        </div>

        <!-- <ul>
        <li><a href="">Beranda</a></li>
        <li> <a href="">Kursus Saya</a></li>
    </ul> -->
        </div>
        <div class="main">
            <h1 style="text-align: center; font-size:50px; margin-top:8%">Edit Profile</h1><br>


            <form method="post" action="config/update_user.php">
                <div class="form-group" style="margin-left:10%">
                    <label for="email">Alamat Email</label>
                    <input type="hidden" name="id" value="<?php echo $d['id']; ?>">
                    <input type="email" class="form-control" name="email" aria-describedby="emailHelp" value="<?php echo $d['email']; ?> " style="width:80%">
                </div>

                <div class="form-group" style="margin-left:10%">
                    <label for="LongName">Nama Lengkap</label>
                    <input type="text" class="form-control" name="LongName" value="<?php echo $d['nama_lengkap']; ?>" style="width:80%">
                </div>

                <div class="form-group" style="margin-left:10%">
                    <label for="NickName">Nama Panggilan</label>
                    <input type="text" class="form-control" name="NickName" value="<?php echo $d['nama_panggilan']; ?>" style="width:80%">
                </div>

                <div class="form-group" style="margin-left:10%">
                    <label for="ttl">Tanggal Lahir</label> <br>
                    <input class="form-control" type="text" id="datepicker" style="visibility: visible; width: 80%" name="TLahir" value="<?php echo $d['tgl_lahir']; ?>">
                    <button type="button" class="btn btn-secondary" onclick="datePick()" id="dateButton">Pick Date</button>
                    <!-- <input type="text" class="form-control" name="ttl" value="<?php echo $d['tgl_lahir']; ?>" style="width:80%"> -->
                </div>

                <div class="form-group" style="margin-left:10%">
                    <label for="password">Password</label>
                    <input type="password" class="form-control" name="password" placeholder="Masukan Password" style="width:80%">
                </div>

                <div class="form-group" style="margin-left:10%">
                    <label for="konfirm">Konfirmasi Password</label>
                    <input type="password" class="form-control" name="konfirm" placeholder="Konfirmasi Password" style="width:80%">
                </div>
                <br>
                <button type="submit" class="btn btn-primary" style="margin-left:45%" name="update">Submit</button>
            </form>

        <?php
        }
        ?>
        </div>
        </table>
        </div>
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.18.6/dist/sweetalert2.all.min.js"></script>
        <script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    </body>

    </html>