<?php
include "config/connection.php";
session_start();
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Daftar Pengajar</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
<style>
  body {font-family: "Lato", sans-serif;}

.sidebar {
  height: 100%;
  width: 200px;
  position: fixed;
  z-index: 1;
  top: 0;
  left: 0;
  background-color:#24d4d4;
  overflow-x: hidden;
  padding-top: 16px;
  margin-top: 4%;
  

}

.sidebar a {
  padding: 6px 8px 6px 16px;
  text-decoration: none;
  font-size: 20px;
  color:floralwhite;
  display: block;
}

.sidebar a:hover {
  color: cadetblue;
}
.sidebar a:active {
  color: cadetblue;
}

.main {
  margin-left: 160px; /* Same as the width of the sidenav */
  padding: 0px 10px;
  margin-top: 4%;
}

@media screen and (max-height: 450px) {
  .sidebar {padding-top: 15px;}
  .sidebar a {font-size: 18px;}
}
.nav-link :hover{
  color: white;
}
</style>
</head>
<body>


<nav class="navbar fixed-top navbar-light" style="background-color: #00D8D6;">
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample01" aria-controls="navbarsExample01" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <a href="#" style="color: white; font-size: 200%; float: left;">BelajarYuk!</a>
  
  <div class="collapse navbar-collapse" id="navbarsExample01">
    <ul class="navbar-nav" id="navbarsExample01">
      <li class="nav-item active">
        <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Menu Kursus.html">Kursus</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Menu Programs.html">Program</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Pengajar.html">Pengajar</a>
      </li>
    </ul>
  </div>

    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white;">
      Account
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
      <a class="dropdown-item" href="#">Profil</a>
      <a class="dropdown-item" href="#">Daftar user</a>
      <a class="dropdown-item" href="#">Keluar</a>
    </div>
  
  
</nav>

<br>
<div class="sidebar">
  
 <a href="#home"><i class="fa fa-fw fa-home"></i> Home</a> 
  <a href="admin_user.php"><i class="fa fa-fw fa-wrench"></i>Daftar User</a>
  <a href="daftar_pengajar.php"><i class="fa fa-fw fa-wrench"></i>Daftar Pengajar</a>
</div>
</div>
<div class="main">
<h1 style="text-align: center; font-size:50px; margin-top:8%" >DAFTAR PENGAJAR</h1><br>
<button class="btn btn-primary" data-toggle="modal" data-target="#register" style="position: relative; left:45%">Tambah Pengajar</button> <br> <br>
<!-- Register Mentor-->
<div class="modal fade" id="register" role="dialog">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Register Pengajar</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <form action="register_pengajar.php" method="POST">
                        <div class="modal-body">
                            <label>Email Address Pengajar</label>
                            <div class="input-group flex-nowrap">
                                <input type="email" name="email" class="form-control" placeholder="Masukan Email.." aria-label="Email" aria-describedby="addon-wrapping">
                            </div><br>
                            <label>Nama Lengkap Pengajar</label>
                            <div class="input-group flex-nowrap">
                                <input type="text" name="NamaLengkap" class="form-control" placeholder="Masukan Nama Lengkap Pengajar.." aria-label="Nama Lengkap" aria-describedby="addon-wrapping">
                            </div><br>
                            <label>Password</label>
                            <div class="input-group flex-nowrap">
                                <input type="password" name="Password" class="form-control" placeholder="Masukan Password" aria-label="Password" aria-describedby="addon-wrapping">
                            </div><br>
                            <label>Confirm Password</label>
                            <div class="input-group flex-nowrap">
                                <input type="password" name="repass" class="form-control" placeholder="Confirm Password" aria-label="Confirm Password" aria-describedby="addon-wrapping">
                            </div>
                        </div>
                        <div class=" modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <input type="submit" name="register" class="btn btn-primary" value="Register">
                        </div>
                    </form>
                </div>
            </div>
        </div>
<div style="width:70%;margin:auto;">
    <table class="table table-striped">
  <thead>
    <tr>
      <th scope="col">No</th>
      <th scope="col">Nama Pengajar</th>
      <th scope="col">Email Pengajar</th>
      <th scope="col">Password</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>

  <?php
 $query = mysqli_query($conn,"select * from mentor");
  $nomor = 1;
  while($d = mysqli_fetch_array($query)){

  ?>

<tr>
<td><?php echo $nomor++; ?></td>
        <td><?php echo $d['nama_lengkap_mentor']; ?></td>
        <td><?php echo $d['email_mentor']; ?></td>
        <td><?php echo $d['password']; ?></td>
				<td>
					<a href="edit_profile_pengajar.php?id=<?php echo $d['id']; ?>">EDIT</a>
					<a href="config/delete_pengajar.php?id=<?php echo $d['id']; ?>">HAPUS</a>
				</td>

</tr>
</tbody>
<?php 
		}
		?>
</table>
</div>
</div>
</body>
</html>