<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
 
    <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
    <script>
        function datePick() {
            $(function() {
                $("#datepicker").datepicker();
                $("#datepicker").datepicker('show');
                document.getElementById("datepicker").style.visibility = 'visible';
            });
        }

        function done() {
            var pertanyaan = confirm("Apakah anda yakin?");
            if (pertanyaan == false) {
                event.preventDefault();
            }
        }
    </script>
    <title>BelajarYuk</title>
    <style>
        body {
            background: linear-gradient(rgba(0, 0, 0, 0.7),
                    rgba(0, 0, 0, 0.7)), url('img/background-landing-page.jpg');
            /* background-image: url('img/background-landing-page.jpg'); */
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: cover;
        }
    </style>
</head>

<body>
    <div class="container" style="background-image: url('img/background-landing-page.jpg');
            background-repeat: no-repeat;
            background-attachment: fixed;
            background-size: cover;">
        <nav class="navbar fixed-top" style="background-color: ;">
            <a class="navbar-brand" href="#" style="font-family: 'Rubik', sans-serif; font-size: 30px; font-weight: 900; color: white">BelajarYuk!</a>
            <button class="navbar-toggler" type="button" data-toggler="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle Navigation">
                <span class="navbar-toggler-icon"></span>
            </button>


            <ul class="nav justify-content-end" style="font-family: 'Rubik', sans-serif; font-size: 22px;">
                <li class="nav-item">
                    <a class="nav-link" href="Halaman-home.php" style="color: white; border-bottom: 2px solid white">Home
                        <span class="sr-only">(current)</span></a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="Menu Kursus.php" style="color: white;">Kursus
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="Menu Programs.php" style="color: white">Program</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="Menu Teacher.php" style="color: white">Pengajar</a>
                </li>

            </ul>

        </nav>
        <!-- Login -->
        <div class="modal fade" id="login" role="dialog">
            <div class="modal-dialog modal-sm modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Login</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <form action="config/login.php" method="POST">
                        <div class="modal-body">
                            <label>Email Address</label>
                            <div class="input-group flex-nowrap">
                                <input type="text" name="email" class="form-control" placeholder="Masukan Email" aria-label="Email" aria-describedby="addon-wrapping">
                            </div><br>
                            <label>Password</label>
                            <div class="input-group flex-nowrap">
                                <input type="password" name="password" class="form-control" placeholder="Masukan Password" aria-label="Password" aria-describedby="addon-wrapping">
                            </div>
                        </div>
                        <div class=" modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <input type="submit" name="submit" class="btn btn-primary" value="Login">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- Register -->
        <div class="modal fade" id="register" role="dialog">
            <div class="modal-dialog modal-lg modal-dialog-centered">
                <div class="modal-content">
                    <div class="modal-header">
                        <h4 class="modal-title">Register</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                    </div>
                    <form action="register.php" method="POST">
                        <div class="modal-body">
                            <label>Email Address</label>
                            <div class="input-group flex-nowrap">
                                <input type="email" name="Email" class="form-control" placeholder="Masukan Email.." aria-label="Email" aria-describedby="addon-wrapping">
                            </div><br>
                            <label>Nama Lengkap</label>
                            <div class="input-group flex-nowrap">
                                <input type="text" name="NamaLengkap" class="form-control" placeholder="Masukan Nama Lengkapmu.." aria-label="Nama Lengkap" aria-describedby="addon-wrapping">
                            </div><br>
                            <label>Nama Panggilan</label>
                            <div class="input-group flex-nowrap">
                                <input type="text" name="NamaPanggilan" class="form-control" placeholder="Masukan Nama Panggilanmu.." aria-label="Nama Panggilan" aria-describedby="addon-wrapping">
                            </div><br>
                            <label>Tanggal Lahir (MM/DD/YYYY)</label>
                            <div class="input-group flex-nowrap">
                                <button type="button" class="btn btn-secondary" onclick="datePick()" id="dateButton">Pick
                                    Date</button>
                                    <input class="form-control" type="text" id="datepicker" style="visibility: visible; border: hidden;" name="TLahir">
                                
                            </div><br>

                            <label>Password</label>
                            <div class="input-group flex-nowrap">
                                <input type="password" name="Password" class="form-control" placeholder="Masukan Password" aria-label="Password" aria-describedby="addon-wrapping">
                            </div><br>
                            <label>Confirm Password</label>
                            <div class="input-group flex-nowrap">
                                <input type="password" name="repass" class="form-control" placeholder="Confirm Password" aria-label="Confirm Password" aria-describedby="addon-wrapping">
                            </div>
                        </div>
                        <div class=" modal-footer">
                            <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                            <input type="submit" name="register" class="btn btn-primary" value="Register">
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- Tagline -->
        <p style="position: absolute; width: 471px; height: 100px; left: 83px; top: 180px; font-family: Rubik; font-style: normal; font-weight: 650; font-size: 42px; line-height: 50px;color: #FFFFFF;">
            Belajar dari yang sudah berpengalaman. <br>

            <br>
        </p>

        <a href="#" style="position: absolute;
width: 170px;
height: 50px;
left: 83px;
top: 541px; font-size: 18pt;" class="btn btn-primary text-center" data-toggle="modal" data-target="#login">Login</a>
        <a href="dashboard-home.php" style="position: absolute;
width: 170px;
height: 50px;
left: 83px;
top: 600px; font-size: 18pt;" class="btn btn-primary text-center">Dashboard</a>
        <a href="#" style="position: absolute;
width: 170px;
height: 50px;
left: 300px;
top: 541px; font-size: 18pt;" class="btn btn-primary text-center" data-toggle="modal" data-target="#register">Register</a>
        <!-- Form -->
        <!-- <form action="register.php" method="post">
        <div style="position: absolute;
width: 538px;
height: 538px;
left: 83px;
top: 330px;

background: #FFFFFF;
border-radius: 30px;">
            <p style="position: absolute;
width: 200px;
height: 50px;
left: 50px;
top: 40px;

font-family: Rubik;
font-style: normal;
font-weight: 300;
font-size: 42px;
line-height: 50px;">Sign Up</p>

        </div>
    </div>
        </form> -->


        <!-- Script CSS -->
        
        <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.18.6/dist/sweetalert2.all.min.js"></script>
        <script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
        
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>

</html>