<?php
include "config/connection.php";
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>Home</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
  <link href="https://code.jquery.com/ui/1.10.4/themes/ui-lightness/jquery-ui.css" rel="stylesheet">
  <script src="https://code.jquery.com/jquery-1.10.2.js"></script>
  <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.js"></script>
  <script>
    function datePick() {
      $(function() {
        $("#datepicker").datepicker();
        $("#datepicker").datepicker('show');
        document.getElementById("datepicker").style.visibility = 'visible';
      });
    }
  </script>
  <style>
    body {
      font-family: "Lato", sans-serif;
    }

    .sidebar {
      height: 100%;
      width: 200px;
      position: fixed;
      z-index: 1;
      top: 0;
      left: 0;
      background-color: #24d4d4;
      overflow-x: hidden;
      padding-top: 16px;
      margin-top: 4%;


    }

    .sidebar a {
      padding: 6px 8px 6px 16px;
      text-decoration: none;
      font-size: 20px;
      color: floralwhite;
      display: block;
    }

    .sidebar a:hover {
      color: cadetblue;
    }

    .sidebar a:active {
      color: cadetblue;
    }

    .main {
      margin-left: 160px;
      /* Same as the width of the sidenav */
      padding: 0px 10px;
      margin-top: 4%;
    }

    @media screen and (max-height: 450px) {
      .sidebar {
        padding-top: 15px;
      }

      .sidebar a {
        font-size: 18px;
      }
    }

    .nav-link :hover {
      color: white;
    }
  </style>
</head>

<body>


  <nav class="navbar fixed-top navbar-light" style="background-color: #00D8D6;">
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarsExample01" aria-controls="navbarsExample01" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <a href="#" style="color: white; font-size: 200%; float: left;">BelajarYuk!</a>

    <div class="collapse navbar-collapse" id="navbarsExample01">
      <ul class="navbar-nav" id="navbarsExample01">
        <li class="nav-item active">
          <a class="nav-link" href="#">Home <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="Menu Kursus.html">Kursus</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="Menu Programs.html">Program</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="Pengajar.html">Pengajar</a>
        </li>
      </ul>
    </div>

    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color: white;">
      Account
    </a>
    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
      <a class="dropdown-item" href="#">Profil</a>
      <a class="dropdown-item" href="#">Daftar</a>
      <a class="dropdown-item" href="#">Keluar</a>
    </div>


  </nav>

  <br>
  <div class="sidebar">

    <a href="#home"><i class="fa fa-fw fa-home"></i> Home</a>
    <a href="Kursus User.html"><i class="fa fa-fw fa-wrench"></i>Daftar</a>


  </div>

  <!-- <ul>
        <li><a href="">Beranda</a></li>
        <li> <a href="">Kursus Saya</a></li>
    </ul> -->
  </div>
  <div class="main">
    <h1 style="text-align: center; font-size:50px; margin-top:8%">Edit Profile</h1><br>

    <?php
    $id = $_GET['id'];
    $data = mysqli_query($conn, "select * from users where id='$id'");
    while ($d = mysqli_fetch_array($data)) {
      ?>

      <form method="post" action="config/update.php">
        <div class="form-group" style="margin-left:10%">
          <label for="email">Alamat Email</label>
          <input type="hidden" name="id" value="<?php echo $d['id']; ?>">
          <input type="email" class="form-control" name="email" aria-describedby="emailHelp" value="<?php echo $d['email']; ?> " style="width:80%">
        </div>

        <div class="form-group" style="margin-left:10%">
          <label for="LongName">Nama Lengkap</label>
          <input type="text" class="form-control" name="LongName" value="<?php echo $d['nama_lengkap']; ?>" style="width:80%">
        </div>

        <div class="form-group" style="margin-left:10%">
          <label for="NickName">Nama Panggilan</label>
          <input type="text" class="form-control" name="NickName" value="<?php echo $d['nama_panggilan']; ?>" style="width:80%">
        </div>

        <div class="form-group" style="margin-left:10%">
          <label for="ttl">Tanggal Lahir</label> <br>
          <input class="form-control" type="text" id="datepicker" style="visibility: visible; width: 80%" name="TLahir" value="<?php echo $d['tgl_lahir']; ?>">
          <button type="button" class="btn btn-secondary" onclick="datePick()" id="dateButton">Pick Date</button>
          <!-- <input type="text" class="form-control" name="ttl" value="<?php echo $d['tgl_lahir']; ?>" style="width:80%"> -->
        </div>

        <div class="form-group" style="margin-left:10%">
          <label for="password">Password</label>
          <input type="password" class="form-control" name="password" placeholder="Masukan Password" style="width:80%">
        </div>

        <div class="form-group" style="margin-left:10%">
          <label for="konfirm">Konfirmasi Password</label>
          <input type="password" class="form-control" name="konfirm" placeholder="Konfirmasi Password" style="width:80%">
        </div>
        <br>
        <button type="submit" class="btn btn-primary" style="margin-left:45%" name="update">Submit</button>
      </form>

    <?php
    }
    ?>
  </div>

  <!-- <script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.18.6/dist/sweetalert2.all.min.js"></script>
        <script src="https://unpkg.com/ionicons@4.5.10-0/dist/ionicons.js"></script>
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script> -->
</body>

</html>