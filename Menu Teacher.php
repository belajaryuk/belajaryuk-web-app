<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Program</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">

</head>

<body>
    <!-- Navbar -->
    <nav class="navbar fixed-top" style="background-color: #00D8D6;">
        <a class="navbar-brand" href="#"
            style="font-family: 'Rubik', sans-serif; font-size: 30px; font-weight: 900; color: white">BelajarYuk!</a>
        <button class="navbar-toggler" type="button" data-toggler="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle Navigation">
            <span class="navbar-toggler-icon"></span>
        </button>


        <ul class="nav justify-content-end" style="font-family: 'Rubik', sans-serif; font-size: 22px;">
            <li class="nav-item">
                <a class="nav-link" href="Halaman-home.php" style="color: white">Home</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="Menu Kursus.php" style="color: white; ">Kursus </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Menu Programs.php" style="color: white">Program</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Menu Teacher.php"
                    style="color: white; border-bottom: 2px solid white">Pengajar<span
                        class="sr-only">(current)</span></a>
            </li>
        </ul>

    </nav>
    <!-- Batas Navbar -->
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="text-center" style="font-family: 'Rubik', sans-serif; font-weight: 700;">
        <h1 style="font-size: 50px"><b>Pengajar Kami</b></h1>
    </div>
    <div class="text-center" style="font-family: 'Rubik', sans-serif; font-size: 24px;">
        <p style="font-weight: 100"><i>Untuk menunjang pembelajaran, kami menawarkan pengajar-pengajar yang professional
                yang dapat membuat pengalaman belajar menyenangkan lho!</i></p>
    </div>
    <br>
    <div class="card-deck" style="padding-block: 200px;">
        <div class="card bg-primary">
          <div class="card-body text-center">
          <img src="/img/benjaminfelix.png">
            <p class="card-text">Pengajar1</p>
          </div>
        </div>
        <div class="card bg-warning">
          <div class="card-body text-center">
          <img src="/img/adhistyzara.png">
            <p class="card-text">Pengajar 2 </p>

            </p>
          </div>
        </div>
        <div class="card bg-success">
          <div class="card-body text-center">
          <img src="/img/anyageraldne.png" >
            <p class="card-text">Pengajar 3</p>
          </div>
        </div>
      </div>
    </body>
</html>