<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Program</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <link href="https://fonts.googleapis.com/css?family=Rubik&display=swap" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

</head>

<body>
    <!-- Navbar -->
    <nav class="navbar fixed-top" style="background-color: #00D8D6;">
        <a class="navbar-brand" href="#"
            style="font-family: 'Rubik', sans-serif; font-size: 30px; font-weight: 900; color: white">BelajarYuk!</a>
        <button class="navbar-toggler" type="button" data-toggler="collapse" data-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle Navigation">
            <span class="navbar-toggler-icon"></span>
        </button>


        <ul class="nav justify-content-end" style="font-family: 'Rubik', sans-serif; font-size: 22px;">
            <li class="nav-item">
                <a class="nav-link" href="Halaman-home.php" style="color: white">Home</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link" href="Menu Kursus.php" style="color: white; ">Kursus </a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Menu Programs.php"
                    style="color: white; border-bottom: 2px solid white">Program<span
                        class="sr-only">(current)</span></a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="Menu Teacher.php" style="color: white">Pengajar</a>
            </li>
        </ul>

    </nav>
    <!-- Batas Navbar -->
    <br>
    <br>
    <br>
    <br>
    <br>
    <div class="text-center" style="font-family: 'Rubik', sans-serif; font-weight: 700;">
        <h1 style="font-size: 50px"><b>Program Kami</b></h1>
    </div>
    <div class="text-center" style="font-family: 'Rubik', sans-serif; font-size: 24px;">
        <p style="font-weight: 100"><i>Untuk menunjang pembelajaran, kami menawarkan program-program yang menarik lho!</i></p>
    </div>
    <!-- <div class="container">
        <div class="row">
            <div class="col">
                <img src="img/undraw_youtube_tutorial_2gn3.svg" alt="gambar yutub" style="width: 612px; height: 451px;">
            </div>
            <div class="col">
                <div class="text-center">
                    <h1>Program Penjelasan via Online Video Tutoring</h1>
                    <p>Program ini memudahkan teman-teman dalam memahami materi</p>
                </div>
            </div>
        </div>
    </div> -->
    <div class="bd-example" style="color:black; margin-left:5%; margin-top:5%; background-color:#00D8D6; margin-right:5%;">
  <div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
    <ol class="carousel-indicators">
      <li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
      <li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
    </ol>
    <div class="carousel-inner">
      <div class="carousel-item active">
        <img src="img/undraw_professor_8lrt.png" class="d-block w-50" alt="...">
        <div class="carousel-caption d-none d-md-block" style="color:black; margin-left:40%;">
          <h5 >Kami terbaik dalam edukasi</h5>
          <p >Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. </p>

        </div>
      </div>
      <div class="carousel-item" >
        <img src="img/undraw_analysis_4jis (7).png" class="d-block w-50" alt="...">
        <div class="carousel-caption d-none d-md-block" style="color:black; margin-left:40%; margin-top:1%" >
          <h5 >Kami Berusaha Menjadi Yang Terbaik</h5>
          <p >Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
        </div>
      </div>
      <div class="carousel-item">
        <img src="img/undraw_predictive_analytics_kf9n.png" class="d-block w-50" alt="...">
        <div class="carousel-caption d-none d-md-block"  style="color:black; margin-left:40%; margin-top:1%">
          <h5 style="color:black;">Pendidikan Adalah Hidup</h5>
          <p style="color:black;">Praesent commodo cursus magna, vel scelerisque nisl consectetur.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. </p>
        </div>
      </div>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
      <span class="carousel-control-prev-icon" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
      <span class="carousel-control-next-icon" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
  </div>
</div>
</body>

</html>